const mogoose = require('mongoose');
const { db } = require('../configuration');

module.exports.connectDb = () => {
    mogoose.connect(db, { useNewUrlParser: true })
    return mogoose.connection;
}