const express = require('express');
const app = express();
const { connectDb } = require('./helpers/db');
const { port, host, db } = require('./configuration');

const startSever = () => {
    app.listen(port, () => {
        console.log(`Started api service on port: ${port}`);
        console.log(`On host: ${host}`);
        console.log(`Our database: ${db}`);
    });
}

app.get('/test', (req, res) => {
    res.send('API service test work');
});

connectDb()
    .on('error', console.log)
    .on('disconnect', connectDb)
    .on('open', startSever)